cd analytics
cd sentiment_extraction
kubectl delete deployment sentiment-pod
kubectl apply -f deployment.yml

cd ..
cd emotion_extraction
kubectl delete deployment emotion-pod
kubectl apply -f deployment.yml

cd ..
cd named_entity_recognition
kubectl delete deployment namedentity-pod
kubectl apply -f deployment.yml

cd ..
cd toxicity_analysis
kubectl delete deployment toxicity-pod
kubectl apply -f deployment.yml
