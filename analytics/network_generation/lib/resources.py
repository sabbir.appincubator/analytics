

class NetworkGraph():
    def __init__(self,**kwargs):
        self.nodes=[]
        self.edges=[]
        self.keywords=kwargs.get('keywords',None)
        self.sources=kwargs.get('sources',None)
        self.feed_source=kwargs.get('feed_source',None)
        self.filter=kwargs.get('filter',None)
        self.user=kwargs.get('user',None)
        self.collection=kwargs.get('collection_name',None)
        self.timestamp=kwargs.get('timestamp',None)
