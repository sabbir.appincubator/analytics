import re

def get_emotion(text,tokenizer,model):
    input_ids = tokenizer.encode(text + '</s>', return_tensors='pt')

    output = model.generate(input_ids=input_ids,
               max_length=2)

    dec = [tokenizer.decode(ids) for ids in output]
    emote=re.search('(?<=\>\s)([a-z]+)',dec[0])[0]
    return emote
