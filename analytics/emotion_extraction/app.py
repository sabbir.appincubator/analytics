import json
import flask
from flask import Flask, request
from transformers import AutoTokenizer, AutoModelWithLMHead
from lib import get_emotion


tokenizer = AutoTokenizer.from_pretrained("mrm8488/t5-base-finetuned-emotion")

model = AutoModelWithLMHead.from_pretrained("mrm8488/t5-base-finetuned-emotion")

app=Flask('emotion_extraction')

@app.route('/process', methods=['POST'])
def process():
    tweet_text=request.headers.get('twt.retweeted_status.extended_tweet.full_text','')
    print(tweet_text)
    if not tweet_text:
        return json.dumps('None')
    else:
        return json.dumps(get_emotion(tweet_text,tokenizer,model))

if __name__=="__main__":
    app.run(host='0.0.0.0',port=5100)
