import json
from flask import Flask, request
from transformers import AutoTokenizer, AutoModelWithLMHead, pipeline

from lib import get_sentiment
model = pipeline("sentiment-analysis", model="finiteautomata/bertweet-base-sentiment-analysis")

app=Flask("sentiment_extraction")

@app.route('/process', methods=['POST'])
def process():
    tweet_text=request.headers.get('twt.retweeted_status.extended_tweet.full_text','')
    print(tweet_text)

    if not tweet_text:
        return json.dumps({'label': 'None', 'score': 0.0})
    else:
        result= get_sentiment(tweet_text,model)
        print(result)
        result['score']=float(result['score'])
        return json.dumps(result)

if __name__=="__main__":
    app.run(host='0.0.0.0',port=5200)
