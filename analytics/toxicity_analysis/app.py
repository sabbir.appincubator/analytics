from flask import Flask, request
from detoxify import Detoxify
import json

from lib import toxicity_assessment

app=Flask('toxicity_assessment')

model = Detoxify("original-small")

@app.route('/process', methods=['POST'])
def process():
    tweet_text=request.headers.get('twt.retweeted_status.extended_tweet.full_text','')
    print(tweet_text)

    if not tweet_text:
        return json.dumps('None')
    else:

        result= toxicity_assessment(tweet_text,model)
        print(result)
        result={key:float(result[key]) for key in result}
        return json.dumps(result)
if __name__=="__main__":
    app.run(host='0.0.0.0',port=5400)
