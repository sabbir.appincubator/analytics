from flask import Flask, request
import json
import torch
from transformers import AutoModel, AutoTokenizer
from lib import normalizeTweet

bertweet = AutoModel.from_pretrained("vinai/bertweet-base")

tokenizer = AutoTokenizer.from_pretrained("vinai/bertweet-base")

app=Flask("NER")

@app.route('/process', methods=['POST'])
def process():
    tweet_text=request.headers.get('twt.retweeted_status.extended_tweet.full_text','')
    print(tweet_text)
    if not tweet_text:
        return json.dumps('None')
    else:
        normalized_text=normalizeTweet(tweet_text)
        input_ids = torch.tensor([tokenizer.encode(normalized_text)])

        with torch.no_grad():
            features = bertweet(input_ids)

        return json.dumps(list(bertweet(tweet_text,tokenizer,model)))

if __name__=="__main__":
    app.run(host='0.0.0.0',port=5500)
