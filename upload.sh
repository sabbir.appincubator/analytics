cd analytics
docker tag sentiment http://167.99.189.196:8083/docker-hosted/sentiment
docker push http://167.99.189.196:8083/docker-hosted/sentiment
cd ..
docker tag emotion http://167.99.189.196:8083/docker-hosted/emotion
docker push http://167.99.189.196:8083/docker-hosted/emotion
cd ..
docker tag named_entity http://167.99.189.196:8083/docker-hosted/named_entity
docker push http://167.99.189.196:8083/docker-hosted/named_entity
cd ..
docker tag toxicity_analysis http://167.99.189.196:8083/docker-hosted/toxicity_analysis
docker push http://167.99.189.196:8083/docker-hosted/toxicity_analysis
cd ..

