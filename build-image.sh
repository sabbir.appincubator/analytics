cd analytics
cd sentiment_extraction
docker build -t sentiment .
cd ..
cd emotion_extraction
docker build -t emotion .

cd ..
cd named_entity_recognition 
docker build -t namedentity .

cd ..
cd toxicity_analysis 
docker build -t toxicity .

cd ..

